#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

int main(int argc, char* argv[])
{
    if( argc < 2 )
    {
        std::cout << "usage: IP" << std::endl;
        return 0;
    }

    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    struct sockaddr_in addrSrv;
    addrSrv.sin_addr.s_addr = inet_addr("127.0.0.1");
    addrSrv.sin_family = AF_INET;
    addrSrv.sin_port = htons(60014);

    std::string message("8900980876,");
    message += argv[1];

    sendto(sockfd, message.c_str(), message.size(), 0, 
        (struct sockaddr*)&addrSrv, sizeof(struct sockaddr));

    char resp[128] = {0};
    socklen_t len = sizeof(struct sockaddr);
    recvfrom(sockfd, resp, sizeof(resp), 
            0, (struct sockaddr*)&addrSrv, &len);
    std::cout << "Pong: " << resp << std::endl;

    close(sockfd);

    return 0;
}
