#!/bin/bash

make clean && make release
[ $? == 0 ] || exit 255

[ -d package ] && rm -rf package/
[ -d package/.libs ] || mkdir -p package/.libs

cp _build/l2tpguard package/ && \
cp /usr/local/lib/libglog.so.0 \
   /opt/talenet/libpcap.so.1 \
   /usr/lib64/libboost_program_options.so.5 \
   /usr/lib64/libboost_system.so.5 \
   /usr/lib64/libboost_filesystem.so.5 \
   package/.libs/

VERSION=`git log -n 1 | grep -oP '\d+\.\d+\.\d+'`
[ "x$VERSION" == "x" ] && echo "Failed to get version number." && exit 255
mv package/ l2tpguard-v$VERSION/
tar -cjf l2tpguard-v$VERSION.tar.bz2 l2tpguard-v$VERSION/
rm -rf l2tpguard-v$VERSION/
