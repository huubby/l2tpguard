#ifndef __COMMON_H__
#define __COMMON_H__

#define PROGRAM_NAME    "L2TP Guard"
#define MAJOR_VERSION   1
#define MINOR_VERSION   0
#define BUILD_NO        5

#define KB(x) ((uint64_t) (x) << 10)
#define MB(x) ((uint64_t) (x) << 20)
#define GB(x) ((uint64_t) (x) << 30)
#define TB(x) ((uint64_t) (x) << 40)

#define VLOG_DEFAULT  1
#define VLOG_NOTICE   2
#define VLOG_DEBUG    3
#define VLOG_TRACE    4


#define ETH_HDR_LEN 14
#define LEAST_VALID_ETH_PKT_LEN ETH_HDR_LEN

#define IP_HDR_LEN 20
#define LEAST_VALID_IP_PKT_LEN (LEAST_VALID_ETH_PKT_LEN + IP_HDR_LEN)

#define UDP_HDR_LEN 8
#define LEAST_VALID_UDP_PKT_LEN (LEAST_VALID_IP_PKT_LEN + UDP_HDR_LEN)


#endif // __COMMON_H__
