#ifndef __L2TP_DISSECTOR_H__
#define __L2TP_DISSECTOR_H__

#include <map>
#include <unordered_set>
#include <mutex>
#include <boost/pool/object_pool.hpp>

struct L2tpSession
{
    uint32_t timestamp; /* In seconds */
    uint32_t lac_ip;
    uint32_t lns_ip;
    uint16_t lns_tunnel_id;
    uint16_t lns_sess_id;
};

struct l2tphdr_flags;
class L2tpDissector
{
    typedef std::map<uint32_t, L2tpSession*> L2tpSessionTable;

public:
    L2tpDissector();
    void Dissect(const uint8_t *packet, uint32_t length);
    void Cleanup(uint32_t timeoutInSec);
    L2tpSession* GetSession(uint32_t key);
    void RemoveSession(uint32_t key, L2tpSession* sess);

    void DumpSessions();

private:
    bool IsMe(const uint8_t *data);
    void DissectMe(const uint8_t *data, uint32_t len);
    uint32_t GetPayloadOffset(const uint8_t* l2tphdr);
    void SaveSession(const uint8_t *data, uint16_t flags, uint32_t key);
    std::string SessionToStr(uint32_t key, L2tpSession* sess);

private:
    mutable std::mutex m_mutex;
    L2tpSessionTable m_sessions;
    boost::object_pool<L2tpSession> m_pool;
};

#endif // __L2TP_DISSECTOR_H__
