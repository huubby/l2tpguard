#include "l2tp_blocker.h"
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <glog/logging.h>
#include "l2tp_dissector.h"
#include "common.h"
#include "Exception.h"


// ----------------------------------------------------------------------
struct L2TP_BLOCK_CMD
{
    uint8_t  magic[2]; //0x1397
    in_addr  lac_addr;
    in_addr  lns_addr;
    uint16_t lns_tunnel_id;
    uint16_t lns_session_id;
};


// ----------------------------------------------------------------------

DEFINE_EXCEPTION(SocketBindFailure);
DEFINE_EXCEPTION(SocketRecvFailure);
DEFINE_EXCEPTION(SocketSelectFailure);
DEFINE_EXCEPTION(SocketSendFailure);
DEFINE_EXCEPTION(InvalidBlockCommand);

L2tpBlocker::L2tpBlocker(L2tpDissector *dissector)
    : m_running(false), m_dissector(dissector), m_notifySocket(-1)
{
}

L2tpBlocker::~L2tpBlocker()
{
    if (m_recvThr && m_recvThr->joinable())
        m_recvThr->join();

    if (m_notifySocket != -1)
        close(m_notifySocket);
}

void L2tpBlocker::Start(uint16_t listenPort,
    const std::string& server, uint16_t notifyPort)
{
    m_notifySocket = socket(AF_INET, SOCK_DGRAM, 0);

    bzero(&m_notifyServer, sizeof(struct sockaddr_in));
    m_notifyServer.sin_family = AF_INET;
    m_notifyServer.sin_addr.s_addr = inet_addr(server.c_str());
    m_notifyServer.sin_port = htons(notifyPort);

    m_running = true;
    m_recvThr.reset(new std::thread(
        std::bind(&L2tpBlocker::Run, this, listenPort)));
}

void L2tpBlocker::Stop()
{
    if (!m_running)
        return ;

    m_running = false;
    if (m_recvThr && m_recvThr->joinable())
        m_recvThr->join();

    if (m_notifySocket != -1)
        close(m_notifySocket);
}

void L2tpBlocker::Run(uint16_t port)
{
    sockaddr_in si;
    si.sin_family = AF_INET;
    si.sin_addr.s_addr = htonl(INADDR_ANY);
    si.sin_port = htons(port);

    int s = socket(AF_INET, SOCK_DGRAM, 0);
    if (bind(s, (struct sockaddr*)&si, sizeof(si)) == -1) {
        LOG(ERROR) << "Failed to bind on port "
            << port << ", " << strerror(errno);
        THROW_EXCEPTION(SocketBindFailure, strerror(errno));
    }

    int flags = fcntl(s, F_GETFL, 0);
    fcntl(s, F_SETFL, flags | O_NONBLOCK);

    LOG(INFO) << "UDP server on port " << port << " is ready to run";

    static const auto IDLE_YIELD = std::chrono::microseconds(100);
    static const uint32_t SESS_TIMEOUT_SEC = 300;
    static const auto SESS_CLEAN_UP_INTERVAL = std::chrono::seconds(60);

    auto begin = std::chrono::steady_clock::now();
    while (m_running) {
        try {
            bool idle = Recv(s);
            if (idle)
                std::this_thread::sleep_for(IDLE_YIELD);

            auto now = std::chrono::steady_clock::now();
            if (now - begin > SESS_CLEAN_UP_INTERVAL) {
                m_dissector->Cleanup(SESS_TIMEOUT_SEC);
                begin = now;
            }
        } catch (...) {
            // Do nothing, everything has been handled downstairs
        }
    }

    LOG(INFO) << "Blocker exited.";
}

bool L2tpBlocker::Recv(int skt)
{
    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(skt, &rfds);

    timeval tv{0, 100};
    int retVal = select(skt + 1, &rfds, nullptr, nullptr, &tv);
    if (retVal == 0) {
        if (VLOG_IS_ON(VLOG_TRACE))
            VLOG(VLOG_TRACE) << "No events, going idle";
    } else if (FD_ISSET(skt, &rfds)) {
        char message[128];
        sockaddr_in peer;
        socklen_t len = sizeof(peer);
        int ret = recvfrom(skt, message,
            sizeof(message), 0, (sockaddr*)&peer, &len);
        if (ret < 0 && errno != EAGAIN) {
            LOG(ERROR) << "Receive data from remote peer faild, "
                << strerror(errno);
            THROW_EXCEPTION(SocketRecvFailure, strerror(errno));
        } else if (ret == sizeof(message)) {
            LOG(WARNING) << "Invalid block command, content too long";
            THROW_EXCEPTION(InvalidBlockCommand, "content too long");
        }

        message[ret] = '\0';
        bool succeed = ProcessMessage(message);
        std::string result(succeed ? "DONE," : "FAIL,");
        result += message;
        SendTo(skt, result.c_str(), result.size(), (sockaddr*)&peer);
    } else {
        LOG(ERROR) << "Check receiving socket failed, " << strerror(errno);
        THROW_EXCEPTION(SocketSelectFailure, strerror(errno));
    }

    return retVal == 0;
}

bool L2tpBlocker::ProcessMessage(const std::string& message)
{
    std::string::size_type pos = message.rfind(",");
    if (pos == std::string::npos) {
        LOG(WARNING) << "Invalid block command " << message;
        return false;
    }

    std::string ipStr = message.substr(pos+1);
    LOG(INFO) << "Block command on user " << ipStr;

    try {
        struct in_addr ip;
        int ret = inet_aton(ipStr.c_str(), &ip);
        if (ret == 0) {
            LOG(WARNING) << "Block command "
                << message << " failed, IP is invalid";
            return false;
        }

        uint32_t key = ntohl(ip.s_addr);
        L2tpSession *sess = m_dissector->GetSession(key);
        if (sess == nullptr) {
            LOG(WARNING) << "IP " << ipStr << " not found";
            return false;
        }

        SendBlockCommand(sess);
        m_dissector->RemoveSession(key, sess);
        LOG(INFO) << "Block command " << message
            << " has been executed successfully.";
        return true;
    } catch (...) {
        LOG(WARNING) << "Block command " << message << " failed, IP is invalid";
    }

    return false;
}

void L2tpBlocker::SendBlockCommand(L2tpSession *sess)
{
    if (m_notifySocket == -1)
        return ;

    L2TP_BLOCK_CMD cmd;
    cmd.magic[0] = 0x13;
    cmd.magic[1] = 0x97;
    cmd.lac_addr.s_addr = htonl(sess->lac_ip);
    cmd.lns_addr.s_addr = htonl(sess->lns_ip);
    cmd.lns_tunnel_id = htons(sess->lns_tunnel_id);
    cmd.lns_session_id = htons(sess->lns_sess_id);

    SendTo(m_notifySocket, &cmd, sizeof(L2TP_BLOCK_CMD),
        (sockaddr*)&m_notifyServer);
}

void L2tpBlocker::SendTo(int socket,
    const void* buf, size_t len, const sockaddr* peer)
{
    static const uint16_t MAX_TRIES = 3;
    uint16_t tried = 0;
    do {
        tried++;
        int ret = sendto(socket, buf, len, 0, peer, sizeof(sockaddr));
        if (ret == -1) {
            LOG(ERROR) << "Sending block command failed, " << strerror(errno);
            THROW_EXCEPTION(SocketSendFailure, strerror(errno));
        } else if ((size_t)ret != len) {
            LOG(ERROR) << "Sending block command partially, will try again";
            if (tried <= MAX_TRIES)
                continue;
        }

        if (tried > MAX_TRIES)
            LOG(ERROR) << "Sending block command partially, retry doesn't fix it";

        break;
    } while (true);
}
