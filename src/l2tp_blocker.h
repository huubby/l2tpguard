#ifndef __L2TP_BLOCKER_H__
#define __L2TP_BLOCKER_H__

#include <stdint.h>
#include <netinet/in.h>
#include <thread>

class L2tpDissector;
struct L2tpSession;

class L2tpBlocker
{
public:
    L2tpBlocker(L2tpDissector *dissector);
    ~L2tpBlocker();

    void Start(uint16_t listenPort,
        const std::string& server, uint16_t notifyPort);
    void Stop();
    bool IsRunning() const { return m_running; }

private:
    void Run(uint16_t port);
    bool Recv(int skt);

    bool ProcessMessage(const std::string& message);
    void SendBlockCommand(L2tpSession *sess);
    void SendTo(int socket, const void* buf, size_t len, const sockaddr* peer);

private:
    bool m_running;
    L2tpDissector *m_dissector;
    int m_notifySocket;
    sockaddr_in m_notifyServer;
    std::shared_ptr<std::thread> m_recvThr;
};

#endif // __L2TP_BLOCKER_H__
