#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <glog/logging.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <pcap.h>
#include "common.h"

#include "l2tp_dissector.h"
#include "l2tp_blocker.h"

/* log configurations/options */
static std::string log_folder("log");
static int log_stderrthreshold=0;
static int log_logbufsecs=5;
static int log_minloglevel=0;
static int log_stop_logging_if_full_disk=1;
static int log_max_log_size=1;
static int g_log_v;

/* program options */
static std::string g_cap_intf_name;
static uint16_t g_listen_port;
static std::string g_input_file_name;

static std::string g_block_server;
static uint16_t g_block_port;

/* packets related */
static pcap_t *g_pcap_handle = nullptr;
static L2tpDissector *g_l2tp_dissector = nullptr;
static L2tpBlocker *g_l2tp_blocker = nullptr;

void pcap_cleanup_handler(int sig)
{
    LOG(INFO) << "Received signal " << sig << ", "
        << PROGRAM_NAME " is about to exit.";

    if (g_pcap_handle)
        pcap_breakloop(g_pcap_handle);

    if (g_l2tp_blocker->IsRunning()) {
        g_l2tp_blocker->Stop();
        LOG(INFO) << PROGRAM_NAME " exited.";
    }
    g_l2tp_dissector = nullptr;
}

void check_block_server(const boost::program_options::options_description &desc)
{
    struct in_addr ip;
    int ret = inet_aton(g_block_server.c_str(), &ip);
    if (ret == 0) {
        std::cout << desc << std::endl;
        exit(0);
    }
}

void check_interface(const boost::program_options::options_description &desc)
{
    pcap_if_t *alldevsp;
    char errbuf[PCAP_ERRBUF_SIZE];

    int ret = pcap_findalldevs(&alldevsp, errbuf);
    if (ret != 0 || alldevsp == NULL) {
        std::cerr << "No interface found, "
            << (ret != 0 ? errbuf : "exit") << std::endl;
        exit(255);
    }

    pcap_if_t *dev = alldevsp;
    while (dev != NULL) {
        if (g_cap_intf_name == dev->name)
            break;

        dev = dev->next;
    }

    if (dev == NULL) {
        std::cerr << "No interface with name "
            << g_cap_intf_name << " found." << std::endl;
        exit(255);
    }

    pcap_freealldevs(alldevsp);
}

void parse_options(int argc, char *argv[])
{
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Show this messages")

        ("interface,i",
         po::value<std::string>(&g_cap_intf_name),
         "On which interface the program will be working")

        ("listen-port,l",
         po::value<uint16_t>(&g_listen_port)->default_value(60014),
         "On which port the program will be waiting for block command");

    po::options_description realDesc(desc);
    realDesc.add_options()
        ("verbose,v", po::value<int>(&g_log_v)->default_value(VLOG_DEFAULT),
         "Verbose logging level")

        ("file,f",
         po::value<std::string>(&g_input_file_name),
         "From where the data will be read")

        ("block-server,s",
         po::value<std::string>(&g_block_server)->default_value("127.0.0.1"),
         "The block server address")

        ("block-port,p",
         po::value<uint16_t>(&g_block_port)->default_value(60016),
         "The block server port");

    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc, argv, realDesc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            exit(0);
        }

        if (!vm.count("interface")) {
            std::cout << desc << std::endl;
            exit(0);
        }

        check_block_server(desc);
        check_interface(desc);
    } catch(...) {
        std::cout << desc << std::endl;
        exit(255);
    }
}

void InitGoogleLogging(const char* program)
{
    FLAGS_stderrthreshold = log_stderrthreshold;
    FLAGS_logbufsecs = log_logbufsecs;
    FLAGS_minloglevel = log_minloglevel;
    FLAGS_stop_logging_if_full_disk = log_stop_logging_if_full_disk;
    FLAGS_max_log_size = log_max_log_size;
    FLAGS_v = g_log_v;

    google::InitGoogleLogging(program);

    namespace bfs = boost::filesystem;
    bfs::path logPath = bfs::path(bfs::current_path());
    logPath /= log_folder;
    if (!bfs::exists(logPath)) {
        bfs::create_directory(logPath);
    }

    bfs::path info = logPath;
    info /= "info_";
    bfs::path warning = logPath;
    warning /= "warning_";
    bfs::path error = logPath;
    error /= "error_";
    google::SetLogDestination(google::INFO, info.string().c_str());
    google::SetLogDestination(google::WARNING, warning.string().c_str());
    google::SetLogDestination(google::ERROR, error.string().c_str());
}

#ifdef MEASURE_PERFORMANCE
static uint64_t g_stat_total_bytes = 0;
static auto g_stat_time_begin = std::chrono::steady_clock::now();
#endif
void got_packet(u_char *args, const pcap_pkthdr *header, const u_char *packet)
{
    if (header->caplen <= LEAST_VALID_UDP_PKT_LEN) {
        VLOG_EVERY_N(VLOG_TRACE, 100) << "Got an invalid packet";
        return ;
    }

    if (!g_l2tp_dissector)
        return ;
    g_l2tp_dissector->Dissect(packet, header->caplen);

#ifdef MEASURE_PERFORMANCE
    namespace sc = std::chrono;
    g_stat_total_bytes += header->len;
    if (g_stat_total_bytes > MB(1)) {
        auto duration = sc::steady_clock::now() - g_stat_time_begin;
        uint64_t micros = sc::duration_cast<sc::microseconds>(duration).count();
        if (micros != 0)
            LOG(INFO) << "Throughput: "
                << (g_stat_total_bytes / (double)micros) * 8 * 1000000
                << " Mbps";
        else
            LOG(INFO) << "Too fast";

        g_stat_total_bytes = 0;
        g_stat_time_begin = sc::steady_clock::now();
    }
#endif
}

void dump_sessions(int sig)
{
    g_l2tp_dissector->DumpSessions();
}

void pump_file()
{
    char errbuf[PCAP_ERRBUF_SIZE];

    LOG(INFO) << "Getting traffic from file " << g_input_file_name;
    pcap_t *file = pcap_open_offline(g_input_file_name.c_str(), errbuf);
    if (file == nullptr) {
        LOG(ERROR) << "Failed to read file "
            << g_input_file_name << ", " << errbuf;
        exit(255);
    }

    while (g_l2tp_dissector != nullptr) {
        pcap_pkthdr header;
        const u_char* packet;
        while ((packet = pcap_next(file, &header)) != NULL)
            got_packet(nullptr, &header, packet);

        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    pcap_close(file);
}

void pump_live()
{
    char errbuf[PCAP_ERRBUF_SIZE];
    LOG(INFO) << "Monitoring traffic on interface " << g_cap_intf_name;

    bpf_u_int32 net, mask;
    int ret =pcap_lookupnet(g_cap_intf_name.c_str(), &net, &mask, errbuf);
    if (ret == -1) {
        LOG(WARNING) << "Cannot get netmask for interface " << g_cap_intf_name;
        net = 0;
        mask = 0;
    }

    static const int SNAP_LEN = 90;
    g_pcap_handle = pcap_open_live(g_cap_intf_name.c_str(),
            SNAP_LEN, 1, 1000, errbuf);

    if (pcap_datalink(g_pcap_handle) != DLT_EN10MB) {
        LOG(ERROR) << "Not a valid data link";
        exit(1);
    }

    /* Make sure only L2TP traffic gets in */
    const char *l2tpFilter = "udp port 1701";
    bpf_program bpf;
    ret = pcap_compile(g_pcap_handle, &bpf, l2tpFilter, 0, net);
    if (ret == -1) {
        LOG(ERROR) << "Filter '" << l2tpFilter
            << "' parsing failed, " << pcap_geterr(g_pcap_handle);
        return ;
    }

    ret = pcap_setfilter(g_pcap_handle, &bpf);
    if (ret == -1) {
        LOG(ERROR) << "Failed to install filter '" << l2tpFilter
            << "', " << pcap_geterr(g_pcap_handle);
        return ;
    }

    pcap_loop(g_pcap_handle, -1, got_packet, NULL);
    pcap_close(g_pcap_handle);
}

int main(int argc, char *argv[])
{
    /* Catch SIGINT and SIGTERM and, if we get either of them, clean up and
     * exit. Do the same with SIGPIPE, in case, for example, we're writing to
     * our standard output and it's a pipe. Do the same with SIGHUP if it's not
     * being ignored (if we're being run under nohup, it might be ignored, in
     * which case we should leave it ignored).
     */
    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = pcap_cleanup_handler;

    /* Arrange that system calls not get restarted, because when our signal
     * handler returns we don't want to restart a call that was waiting for
     * packets to arrive.
     */
    action.sa_flags = 0;
    sigemptyset(&action.sa_mask);
    sigaction(SIGTERM, &action, NULL);
    sigaction(SIGINT, &action, NULL);
    sigaction(SIGPIPE, &action, NULL);

    struct sigaction oldaction;
    sigaction(SIGHUP, NULL, &oldaction);
    if (oldaction.sa_handler == SIG_DFL)
        sigaction(SIGHUP, &action, NULL);

    /* A little bit testing option */
    action.sa_handler = dump_sessions;
    sigaction(SIGALRM, &action, NULL);

    /* Some initializations */
    parse_options(argc, argv);
    InitGoogleLogging(argv[0]);

    LOG(INFO) << "========= "
        << PROGRAM_NAME " is starting up, version: "
        << MAJOR_VERSION << "." << MINOR_VERSION << " build " << BUILD_NO
        << " =========";
    LOG(INFO) << "Listen on port " << g_listen_port << " for block command";
    LOG(INFO) << "Block executor is at "
        << g_block_server << ":" << g_block_port;

    L2tpDissector dissector;
    g_l2tp_dissector = &dissector;

    L2tpBlocker blocker(g_l2tp_dissector);
    g_l2tp_blocker = &blocker;
    blocker.Start(g_listen_port, g_block_server, g_block_port);

    g_input_file_name.length() > 0 ? pump_file() : pump_live();
    LOG(INFO) << PROGRAM_NAME " exited";

    return 0;
}
