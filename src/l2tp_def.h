#ifndef __L2TP_DEF_H__
#define __L2TP_DEF_H__

#include <stdint.h>
#include "common.h"

/*
 * 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |T|L|x|x|S|x|O|P|x|x|x|x|  Ver  |          Length (opt)         |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |           Tunnel ID           |           Session ID          |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |             Ns (opt)          |             Nr (opt)          |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |      Offset Size (opt)        |    Offset pad... (opt)        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 */


#define L2TP_PORT   1701
#define L2TP_OFFSET (ETH_HDR_LEN + IP_HDR_LEN + UDP_HDR_LEN)

#define L2TP_FLAGS_LEN      2 // 2 bytes for various flags
#define L2TP_LENGTH_LEN     2
#define L2TP_TUNNEL_ID_LEN  2

#define CONTROL_BIT(flags)        (flags & 0x8000) /* Type bit control = 1 data = 0 */
#define LENGTH_BIT(flags)         (flags & 0x4000) /* Length bit = 1  */
#define RESERVE_BITS(flags)       (flags & 0x37F8) /* Reserved bit - unused */
#define SEQUENCE_BIT(flags)       (flags & 0x0800) /* SEQUENCE bit = 1 Ns and Nr fields */
#define OFFSET_BIT(flags)         (flags & 0x0200) /* Offset */
#define PRIORITY_BIT(flags)       (flags & 0x0100) /* Priority */
#define L2TP_VERSION(flags)       (flags & 0x000f) /* Version of l2tp */

#define L2TP_VER 2

#define PPP_HDR_LEN         4
#define PPP_PROTO_OFFSET    2
#define PPP_HDR_IP_PROTO    0x0021


#endif // __L2TP_DEF_H__


