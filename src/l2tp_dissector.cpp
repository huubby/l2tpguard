#include "l2tp_dissector.h"
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <glog/logging.h>
#include "common.h"
#include "l2tp_def.h"

// ------------------------------------------------------------
uint16_t buf_get_ntohs(const uint8_t* buf, uint32_t offset)
{
    return ntohs(*(uint16_t*)(buf + offset));
}

// ------------------------------------------------------------

L2tpDissector::L2tpDissector()
{
}

bool L2tpDissector::IsMe(const uint8_t *data)
{
    udphdr *udp = (udphdr*)data;
    /* XXX not necessarily true, but just put it here for now */
    if (ntohs(udp->source) != L2TP_PORT && ntohs(udp->dest) != L2TP_PORT)
        return false;

    uint16_t flags = buf_get_ntohs(data, sizeof(udphdr));
    /* All we can do here is to discriminate between L2TP and L2F */
    return L2TP_VERSION(flags) == L2TP_VER;
}

void L2tpDissector::Dissect(const uint8_t *packet, uint32_t length)
{
    if (length < LEAST_VALID_UDP_PKT_LEN)
        return ;

    udphdr *udp = (udphdr*)(packet + LEAST_VALID_IP_PKT_LEN);
    if (!IsMe((const uint8_t*)udp)) {
        VLOG_EVERY_N(VLOG_TRACE, 256) << "Not a L2TP packet";
        return ;
    }

    DissectMe(packet, length);
}

void L2tpDissector::DissectMe(const uint8_t *packet, uint32_t length)
{
    if (length <= L2TP_OFFSET + L2TP_FLAGS_LEN)
        return;

    uint16_t flags = buf_get_ntohs(packet, L2TP_OFFSET);
    /* Look at data message only */
    if (CONTROL_BIT(flags))
        return ;

    uint32_t pppBeginPos = GetPayloadOffset(packet);
    if (length <= pppBeginPos + PPP_HDR_LEN + IP_HDR_LEN)
        return ;

    uint16_t ppp_proto = buf_get_ntohs(packet, pppBeginPos + PPP_PROTO_OFFSET);
    if (ppp_proto != PPP_HDR_IP_PROTO)
        return ;

    iphdr *innerIp = (iphdr*)(packet + pppBeginPos + PPP_HDR_LEN);
    SaveSession(packet, flags, ntohl(innerIp->saddr));
}

uint32_t L2tpDissector::GetPayloadOffset(const uint8_t* packet)
{
    /* Of course after these flags */
    uint32_t pos = L2TP_OFFSET + L2TP_FLAGS_LEN;

    const uint8_t* l2tphdr = packet + L2TP_OFFSET;
    uint16_t flags = buf_get_ntohs(l2tphdr, 0);
    if (LENGTH_BIT(flags))
        pos += 2; /* Length field */

    pos += 4; /* Tunnel id and session id fields */

    if (SEQUENCE_BIT(flags))
        pos += 4; /* Ns and Nr fields */

    if (OFFSET_BIT(flags)) {
        pos += 2; /* Offset Size field */
        pos += buf_get_ntohs(l2tphdr, pos);
    }

    return pos;
}

void L2tpDissector::SaveSession(const uint8_t *packet, uint16_t flags, uint32_t key)
{
    L2tpSession *sess = nullptr;
    {
        std::lock_guard<std::mutex> guard(m_mutex);
        sess = m_pool.construct();
        sess->timestamp = time(NULL);
    }

    iphdr *outterIp = (iphdr*)(packet + ETH_HDR_LEN);
    sess->lac_ip = ntohl(outterIp->saddr);
    sess->lns_ip = ntohl(outterIp->daddr);

    uint32_t tunOffset = L2TP_OFFSET + L2TP_FLAGS_LEN;
    tunOffset += LENGTH_BIT(flags) ? L2TP_LENGTH_LEN : 0;
    sess->lns_tunnel_id = buf_get_ntohs(packet, tunOffset);

    uint32_t sessOffset = tunOffset + L2TP_TUNNEL_ID_LEN;
    sess->lns_sess_id = buf_get_ntohs(packet, sessOffset);

    std::lock_guard<std::mutex> guard(m_mutex);
    if (m_sessions.count(key)) {
        m_pool.destroy(m_sessions[key]);
    }
    m_sessions[key] = sess;
}

void L2tpDissector::Cleanup(uint32_t timeoutInSec)
{
    uint32_t erased = 0;

    std::lock_guard<std::mutex> guard(m_mutex);
    uint32_t now = time(NULL);
    auto it = m_sessions.begin();
    while (it != m_sessions.end()) {
        if (now < it->second->timestamp ||
                now - it->second->timestamp > timeoutInSec) {
            if (VLOG_IS_ON(VLOG_TRACE))
                VLOG(VLOG_TRACE) << "Session "
                    << SessionToStr(it->first, it->second)
                    << " has been timeout and removed.";
            it = m_sessions.erase(it);
            ++erased;
        } else {
            ++it;
        }
    }

    if (VLOG_IS_ON(VLOG_DEBUG) && erased > 0)
        VLOG(VLOG_DEBUG) << "Erased " << erased << " timeout sessions.";
}

L2tpSession* L2tpDissector::GetSession(uint32_t key)
{
    std::lock_guard<std::mutex> guard(m_mutex);
    if (m_sessions.count(key)) {
        return m_sessions[key];
    }

    return nullptr;
}

void L2tpDissector::RemoveSession(uint32_t key, L2tpSession* sess)
{
    std::lock_guard<std::mutex> guard(m_mutex);
    m_pool.destroy(sess);
    if (m_sessions.count(key)) {
        m_sessions.erase(key);
    }
}


#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
std::string L2tpDissector::SessionToStr(uint32_t key, L2tpSession* sess)
{
    std::string sessStr("{'");

    struct in_addr ip;
    ip.s_addr = htonl(key);
    char* ipStr = inet_ntoa(ip);
    sessStr += ipStr;
    sessStr += "': {";

    ip.s_addr = htonl(sess->lac_ip);
    ipStr = inet_ntoa(ip);
    sessStr += "'lac': '";
    sessStr += ipStr;
    sessStr += "'";

    ip.s_addr = htonl(sess->lns_ip);
    ipStr = inet_ntoa(ip);
    sessStr += ", 'lns': '";
    sessStr += ipStr;
    sessStr += "'";

    sessStr += ", 'lns_tunnel': '";
    sessStr += std::to_string(sess->lns_tunnel_id);
    sessStr += "'";

    sessStr += ", 'lns_session_id': '";
    sessStr += std::to_string(sess->lns_sess_id);
    sessStr += "'";

    sessStr += ", 'timestamp': '";
    sessStr += std::to_string(sess->timestamp);
    sessStr += "'";

    sessStr += "}";
    sessStr += "}";

    return sessStr;
}

#include <fstream>
void L2tpDissector::DumpSessions()
{
    std::fstream s("l2tp_sessions.txt", s.trunc | s.out);

    std::lock_guard<std::mutex> guard(m_mutex);
    auto it = m_sessions.begin();
    for (; it != m_sessions.end(); ++it) {
        s << SessionToStr(it->first, it->second) << std::endl;
    }
    s.close();
}

